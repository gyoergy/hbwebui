import hbwebui

class DbRouter(object):
    def db_for_read(self, model, **hints):
        try:
            return model.hbbackend
        except AttributeError:
            return None

    def db_for_write(self, model, **hints):
        return None

    def allow_relation(self, obj1, obj2, **hints):
        return None

    def allow_syncdb(self, db, model):
        if db == 'hbstats':
            return False
        if db == 'hbbackend':
            return False
        return None
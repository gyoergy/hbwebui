from functools import wraps
from django.utils.decorators import available_attrs

from django.http import HttpResponse, HttpResponseForbidden, HttpResponseBadRequest
from django.template import loader, Context, RequestContext

import re

def login_required_simple(func):
    @wraps(func, assigned=available_attrs(func))
    def inner(request, *args, **kwargs):
        if not request.user.is_authenticated(): return HttpResponseForbidden()
        return func(request, *args, **kwargs)
    return inner

def ident_required(func, simple=False):
    @wraps(func, assigned=available_attrs(func))
    def inner(request, *args, **kwargs):
        try:
            idents = request.session['idents']
        except KeyError:
            idents = None
        if not idents:
            if simple:
                return HttpResponseBadRequest('no idents for %s' % request.user)
            else:
                t = loader.get_template('error_noidents.html')
                c = RequestContext(request, {
                
                })
                return HttpResponse(t.render(c));
        else:
            return func(request, *args, **kwargs)
    return inner

def ident_required_simple(func):
    return ident_required(func, simple=True)

def handle_filter(name, r):
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def inner(request, *args, **kwargs):
            filter_exp = request.GET.get('filter')
            filters_map = kwargs.get('filters_map', {})
            if filter_exp:
                cond_sep = '/'
                k_sep = ':'
                v_sep = ','
                
                try:
                    for cond in filter(None, filter_exp.split(cond_sep)):
                        k, v = cond.split(k_sep, 1)
                        if k != name:
                            continue
                        v = filter(None, v.split(v_sep))
                        if v:
                            for s in v:
                                if not re.match(r, s):
                                    return HttpResponseBadRequest("invalid value in filter condition '%s': '%s'" % (cond, s))
                            filters_map[k] = v
                except ValueError:
                    return HttpResponseBadRequest("error parsing filter condition: '%s'" % cond)
            
            kwargs.update({'filters_map': filters_map})
            return func(request, *args, **kwargs)
        return inner
    return decorator

from hbwebui.models import IdentToGroupMapping
from django.contrib import admin

class IdentToGroupMappingAdmin(admin.ModelAdmin):
    list_display = ('ident', 'group')
    list_filter = list_display
    ordering = ['group', 'ident']

admin.site.register(IdentToGroupMapping, IdentToGroupMappingAdmin)
mod_stats_idents = (function() {
    
    function summary(container, url) {
        mod_stats_base.summary.call(this, container, url);
                
        this.tpl = this.tpl = '\
            <h3><%= n_count %> <% print(pluralize(n_count, "Ident")) %></h3>\
            <ul>\
                <li>seen <%= n_binary %> <% print(pluralize(n_binary, "binary", "binaries")) %></li>\
                <li>from <%= n_source_ip %> <% print(pluralize(n_source_ip, "source IP")) %></li>\
                <li>from <%= n_asn %> <% print(pluralize(n_asn, "ASN")) %></li>\
                <li>from <%= n_cc %> <% print(pluralize(n_cc, "country", "countries")) %></li>\
                <li>on <%= n_target_port %> target <% print(pluralize(n_target_port, "port")) %></li>\
                <li>between <%= ts_first %> - <%= ts_last %></li>\
                <li>in <%= n_total %> <% print(pluralize(n_total, "attack")) %> in total</li>\
            </ul>';
    }
    summary.prototype = utils.extend(
        mod_stats_base.summary,
        {
            renderer: function(data) {
                data.ts_first = mod_base.renderTimestamp(data.ts_first);
                data.ts_last = mod_base.renderTimestamp(data.ts_last);
                mod_stats_base.summary.prototype.renderer.call(this, data);
            }
        }
    );
    
    
    function table(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'Ident', 'mDataProp': 'ident', 'sType': 'string', 'aTargets': [0], 'fnRender': function(o, val) { return '<a href="'+mod_stats_base.url_stats_ident+'/'+val+'"/>'+val+'</a>'; } },
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [1] },
                    { 'sTitle': '# Binaries', 'mDataProp': 'n_binary', 'sType': 'numeric', 'aTargets': [2] },
                    { 'sTitle': '# Source IPs', 'mDataProp': 'n_source_ip', 'sType': 'numeric', 'aTargets': [3] },
                    { 'sTitle': '# ASNs', 'mDataProp': 'n_asn', 'sType': 'numeric', 'aTargets': [4] },
                    { 'sTitle': '# Countries', 'mDataProp': 'n_cc', 'sType': 'numeric', 'aTargets': [5] },
                    { 'sTitle': '# Target Ports', 'mDataProp': 'n_target_port', 'sType': 'numeric', 'aTargets': [6] },
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [7], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [8], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7, 8] },
                ],
                'aaSorting': [
                    [1, 'desc']
                ]
            }
        );
    }
    table.prototype = utils.extend(mod_stats_base.table);
    
    
    return {
        summary: summary,
        table: table
    };
})();
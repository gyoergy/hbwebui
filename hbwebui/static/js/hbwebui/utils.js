utils = (function() {
    
    function extend(base, methods) {
            var proto;
            function Extended() {};
            
            Extended.prototype = base.prototype;
            proto = new Extended();
                        
            for (var m in methods) {
                proto[m] = methods[m];
            }
            
            return proto;
    }
    
    return {
        extend: extend
    };
})();
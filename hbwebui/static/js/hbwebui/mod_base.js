mod_base = (function() {
    
    var timeFormat = d3.time.format("%Y-%m-%d %H:%M");
    function convertTimestamp(ts) { return ts*1000; }
    function renderTimestamp(ts) { return timeFormat(new Date(convertTimestamp(ts))); }
    
    return {
        timeFormat: timeFormat,
        convertTimestamp: convertTimestamp,
        renderTimestamp: renderTimestamp
    };
})();
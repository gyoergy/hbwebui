mod_stats_asns = (function() {
    
    function summary(container, url) {
        mod_stats_base.summary.call(this, container, url);
                
        this.tpl = this.tpl = '\
            <h3><%= n_count %> <% print(pluralize(n_count, "ASN")) %></h3>\
            <ul>\
                <li>in <%= n_cc %> <% print(pluralize(n_cc, "Country", "Countries")) %></li>\
                <li>containing <%= n_source_ip %> <% print(pluralize(n_source_ip, "Source IP")) %></li>\
                <li>collected by <%= n_ident %> <% print(pluralize(n_ident, "ident")) %></li>\
                <li>on <%= n_target_port %> target <% print(pluralize(n_target_port, "port")) %></li>\
                <li>uploaded <%= n_binary %> <% print(pluralize(n_binary, "Binary", "Binaries")) %></li>\
                <li>between <%= ts_first %> - <%= ts_last %></li>\
                <li>in <%= n_total %> <% print(pluralize(n_total, "attack")) %> in total</li>\
            </ul>';
    }
    summary.prototype = utils.extend(
        mod_stats_base.summary,
        {
            renderer: function(data) {
                data.ts_first = mod_base.renderTimestamp(data.ts_first);
                data.ts_last = mod_base.renderTimestamp(data.ts_last);
                mod_stats_base.summary.prototype.renderer.call(this, data);
            }
        }
    );
    
    
    function table(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'AS Name', 'mDataProp': 'as_name', 'sType': 'string', 'aTargets': [0], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; var n = val; if (n.length > 16) n = n.slice(0,16) + '...'; return '<a href="'+mod_stats_base.url_stats_asn+'/'+o.aData.asn+'">'+n+'</a>'; } },
                    { 'sTitle': 'ASN', 'mDataProp': 'asn', 'sType': 'string', 'aTargets': [1], 'fnRender': function(o, val) { if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_asn+'/'+val+'">'+val+'</a>'; } },
                    { 'sTitle': 'Country', 'mDataProp': 'cc', 'sType': 'string', 'aTargets': [2], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_country+'/'+val+'"/>'+val+'</a>'; } },
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [3] },
                    { 'sTitle': '# Idents', 'mDataProp': 'n_ident', 'sType': 'numeric', 'aTargets': [4] },
                    { 'sTitle': '# Binaries', 'mDataProp': 'n_binary', 'sType': 'numeric', 'aTargets': [5] },
                    { 'sTitle': '# Source IPs', 'mDataProp': 'n_source_ip', 'sType': 'numeric', 'aTargets': [6] },
                    { 'sTitle': '# Target Ports', 'mDataProp': 'n_target_port', 'sType': 'numeric', 'aTargets': [7] },
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [8], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [9], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] },
                ],
                'aaSorting': [
                    [3, 'desc']
                ]
            }
        );
    }
    table.prototype = utils.extend(mod_stats_base.table);
    
    
    return {
        summary: summary,
        table: table
    };
})();
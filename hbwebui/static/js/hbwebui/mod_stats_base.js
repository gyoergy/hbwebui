mod_stats_base = (function() {
    
    //FIXME
    var url_stats_binary = '/hbwebui/stats/binary',
        url_stats_ident = '/hbwebui/stats/ident',
        url_stats_source_ip = '/hbwebui/stats/source_ip',
        url_stats_asn = '/hbwebui/stats/asn',
        url_stats_country = '/hbwebui/stats/country';
    
    function buildFilterExp(desc) {
        var conds = []
        
        for (var k in desc) {
            cond = k + ':' + desc[k].join(',');
            conds.push(cond)
        }
            
        return '?filter=' + conds.join('/')
    }
    
    
    function base(container, url) {
        this.container = container;
        this.url = url;
    }
    base.prototype = {
        render: function() {
            $.getJSON(this.url, this.renderer.bind(this));
        },
    };
    
    
    function summary(container, url) {
        base.call(this, container, url);
                
        this.tpl = ''
    }
    summary.prototype = utils.extend(
        base,
        {
            pluralize: function pluralize(n, s, p) {
                if (n == 1)
                    return s;
                else {
                    if (p !== undefined)
                        return p;
                    else
                        return s + 's';
                }
            },
            
            renderer: function(data) {
                var c = $(this.container).empty();
                
                data.pluralize = this.pluralize;                
                c.eq(0).html(_.template(this.tpl, data));
            }
        }
    );
    
    
    function table(container, url) {
        base.call(this, container, url)
        
        this.dtConfig = {
            'bPaginate': false,
            'bLengthChange': false,
            'bFilter': false,
            'bSort': true,
            'bInfo': false,
            'bAutoWidth': false,
            'sDom': '',
            //'sDom': "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            'sPaginationType': 'bootstrap',
        };
        this.dataTable = null;
    }
    table.prototype = utils.extend(
        base,
        {
            renderer: function(data) {
                var c = $(this.container).empty();
                var t = $('<table>').addClass('table').appendTo(c);
                
                this.dtConfig.aaData = data;
                this.dataTable = t.dataTable(this.dtConfig);
            }
        }
    );
    
    
    return {
        url_stats_binary: url_stats_binary,
        url_stats_ident: url_stats_ident,
        url_stats_source_ip: url_stats_source_ip,
        url_stats_asn: url_stats_asn,
        url_stats_country: url_stats_country,
        buildFilterExp: buildFilterExp,
        summary: summary,
        table: table
    };
})();
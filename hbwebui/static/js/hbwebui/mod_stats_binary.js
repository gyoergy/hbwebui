mod_stats_binary = (function() {
    
    function table_idents(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'Ident', 'mDataProp': 'ident', 'sType': 'string', 'aTargets': [0], 'fnRender': function(o, val) { return '<a href="'+mod_stats_base.url_stats_ident+'/'+val+'"/>'+val+'</a>'; } },
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [1] },
                    { 'sTitle': '# Source IPs', 'mDataProp': 'n_source_ip', 'sType': 'numeric', 'aTargets': [2] },
                    { 'sTitle': '# ASNs', 'mDataProp': 'n_asn', 'sType': 'numeric', 'aTargets': [3] },
                    { 'sTitle': '# Countries', 'mDataProp': 'n_cc', 'sType': 'numeric', 'aTargets': [4] },
                    { 'sTitle': '# Target Ports', 'mDataProp': 'n_target_port', 'sType': 'numeric', 'aTargets': [5] },
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [6], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [7], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7] },
                ],
                'aaSorting': [
                    [1, 'desc']
                ]
            }
        );
    }
    table_idents.prototype = utils.extend(mod_stats_base.table);
    
    
    function table_target_ports(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'Target Port', 'mDataProp': 'target_port', 'sType': 'numeric', 'aTargets': [0] },
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [1] },
                    { 'sTitle': '# Idents', 'mDataProp': 'n_ident', 'sType': 'numeric', 'aTargets': [2] },
                    { 'sTitle': '# Source IPs', 'mDataProp': 'n_source_ip', 'sType': 'numeric', 'aTargets': [3] },
                    { 'sTitle': '# ASNs', 'mDataProp': 'n_asn', 'sType': 'numeric', 'aTargets': [4] },
                    { 'sTitle': '# Countries', 'mDataProp': 'n_cc', 'sType': 'numeric', 'aTargets': [5] },                    
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [6], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [7], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7] }
                ],
                'aaSorting': [
                    [1, 'desc'],
                ]
            }
        );
    }
    table_target_ports.prototype = utils.extend(mod_stats_base.table);
    
    
    function table_countries(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'Country', 'mDataProp': 'cc', 'sType': 'string', 'aTargets': [0], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_country+'/'+val+'"/>'+val+'</a>'; } },
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [1] },
                    { 'sTitle': '# Idents', 'mDataProp': 'n_ident', 'sType': 'numeric', 'aTargets': [2] },
                    { 'sTitle': '# Source IPs', 'mDataProp': 'n_source_ip', 'sType': 'numeric', 'aTargets': [3] },
                    { 'sTitle': '# ASNs', 'mDataProp': 'n_asn', 'sType': 'numeric', 'aTargets': [4] },
                    { 'sTitle': '# Target Ports', 'mDataProp': 'n_target_port', 'sType': 'numeric', 'aTargets': [5] },
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [6], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [7], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7] },
                ],
                'aaSorting': [
                    [1, 'desc']
                ]
            }
        );
    }
    table_countries.prototype = utils.extend(mod_stats_base.table);
    
    
    function table_asns(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'AS Name', 'mDataProp': 'as_name', 'sType': 'string', 'aTargets': [0], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; var n = val; if (n.length > 16) n = n.slice(0,16) + '...'; return '<a href="'+mod_stats_base.url_stats_asn+'/'+o.aData.asn+'">'+n+'</a>'; } },
                    { 'sTitle': 'ASN', 'mDataProp': 'asn', 'sType': 'string', 'aTargets': [1], 'fnRender': function(o, val) { if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_asn+'/'+val+'">'+val+'</a>'; } },
                    { 'sTitle': 'Country', 'mDataProp': 'cc', 'sType': 'string', 'aTargets': [2], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_country+'/'+val+'"/>'+val+'</a>'; } },
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [3] },
                    { 'sTitle': '# Idents', 'mDataProp': 'n_ident', 'sType': 'numeric', 'aTargets': [4] },
                    { 'sTitle': '# Source IPs', 'mDataProp': 'n_source_ip', 'sType': 'numeric', 'aTargets': [5] },
                    { 'sTitle': '# Target Ports', 'mDataProp': 'n_target_port', 'sType': 'numeric', 'aTargets': [6] },
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [7], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [8], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7, 8] },
                ],
                'aaSorting': [
                    [3, 'desc']
                ]
            }
        );
    }
    table_asns.prototype = utils.extend(mod_stats_base.table);
    
    
    function table_source_ips(container, url) {
        mod_stats_base.table.call(this, container, url);
        
        this.dtConfig = $.extend(
            this.dtConfig,
            {
                'aoColumnDefs': [
                    { 'sTitle': 'Source IP', 'mDataProp': 'source_ip', 'sType': 'string', 'aTargets': [0], 'fnRender': function(o, val) { return '<a href="'+mod_stats_base.url_stats_source_ip+'/'+val+'"/>'+val+'</a>'; }},
                    { 'sTitle': '# Attacks', 'mDataProp': 'n_count', 'sType': 'numeric', 'aTargets': [1] },
                    { 'sTitle': 'Country', 'mDataProp': 'cc', 'sType': 'string', 'aTargets': [2], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_country+'/'+val+'"/>'+val+'</a>'; } },
                    { 'sTitle': 'AS Name', 'mDataProp': 'as_name', 'sType': 'string', 'aTargets': [3], 'fnRender': function(o, val) { val = val.trim(); if (val == '-') return val; var n = val; if (n.length > 16) n = n.slice(0,16) + '...'; return '<a href="'+mod_stats_base.url_stats_asn+'/'+o.aData.asn+'">'+n+'</a>'; } },
                    { 'sTitle': 'ASN', 'mDataProp': 'asn', 'sType': 'string', 'aTargets': [4], 'fnRender': function(o, val) { if (val == '-') return val; return '<a href="'+mod_stats_base.url_stats_asn+'/'+val+'">'+val+'</a>'; } },
                    { 'sTitle': '# Idents', 'mDataProp': 'n_ident', 'sType': 'numeric', 'aTargets': [5] },
                    { 'sTitle': '# Target Ports', 'mDataProp': 'n_target_port', 'sType': 'numeric', 'aTargets': [6] },
                    { 'sTitle': 'Last', 'mDataProp': 'ts_last', 'sType': null, 'aTargets': [7], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sTitle': 'First', 'mDataProp': 'ts_first', 'sType': null, 'aTargets': [8], 'fnRender': function(o, val) { return mod_base.renderTimestamp(val); }},
                    { 'sDefaultContent': '-', 'aTargets': [0, 1, 2, 3, 4, 5, 6, 7, 8] }
                ],
                'aaSorting': [
                    [1, 'desc'],
                ]
            }
        );
    }
    table_source_ips.prototype = utils.extend(mod_stats_base.table);
        
    
    return {
        summary: null,
        table_idents: table_idents,
        table_target_ports: table_target_ports,
        table_countries: table_countries,
        table_asns: table_asns,
        table_source_ips: table_source_ips
    };
})();
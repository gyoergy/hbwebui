function DB() {
    this.tables =  {};
    
    this.createTable = function(name, csv, defs) {
        var cols, table;
        
        cols = this.csvToCols(csv, defs);
        table = dv.table(
            cols.map(function(col) {
                return {
                    'name': col.name,
                    'values': col.vals,
                    'type': col.type
                };
            })
        );
        
        return this.tables[name] = table;
    };
    
    this.csvToCols = function(csv, defs) {
        var rows = d3.csv.parseRows(csv),
            header = rows.shift(),
            colIndex = header.reduce(function(a,b,i) { a[b] = i; return a; }, {});
        
        var cols = [];
        
        defs.forEach(function(d) {
            var index = colIndex[d.name];
            if (index !== undefined) {
                var col = {};
                col.name = [d.name];
                col.vals = [];
                col.type = d.type;
                col.dType = d.dType;
                
                cols.push(col);
            }
        });
        
        cols.forEach(function(col) {
            var vals = col.vals,
                c = colIndex[col.name];
            
            var f = function(d) { return +d; };
            if (col.dType == 'string') {
                f = function(d) { return d; };
            }
            if (col.dType == 'timestamp_ms') {
                f = function(d) { return d*1000; };
            }
            
            for (var i = 0 ; i < rows.length ; i++) {
                 vals.push(f(rows[i][c]));
             } 
        });
        
        return cols;
    };
    
}
function Table() {
        this.updateableColumns = null;
        this.dataTable = null;
        this.url = null;
        this.keyColumn = null;
        
        this.update = function() {
            $.getJSON(this.url, this.updateTable.bind(this));
        };
        
        this.indexTable = function() {
            var table = this.dataTable;
            
            var index = {};
            
            var n = table.fnSettings().fnRecordsTotal();
            for (var r = 0; r < n ; r++) {
                var rowKey = table.fnGetData(r, 0);
                if (rowKey) {
                    index[rowKey] = r;
                }
            }
            
            return index;
        };
        
        this.updateTable = function (json) {
            var table = this.dataTable,
                keyColumn = this.keyColumn,
                updateableColumns = this.updateableColumns;
            
            var index = this.indexTable();
            
            var loading = 'loading...';
            for (var rowKey in index) {
                for (var c in updateableColumns) {
                    table.fnUpdate(loading, index[rowKey], c);
                }
            }
            table.fnDraw();
            
            $.each(json, function(i, json) {
                var rowKey = json[keyColumn];
                var r = index[rowKey];
                if (r !== undefined) {
                    for (var c in updateableColumns) {
                        table.fnUpdate(json[updateableColumns[c]], r, c);
                        delete index[rowKey];
                    }
                }
            });
            
            for (rowKey in index) {
                for (var c in updateableColumns) {
                    table.fnUpdate(null, index[rowKey], c);
                }
            }
            table.fnDraw();
        };
}
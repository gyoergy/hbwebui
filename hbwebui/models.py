from django.db import models
from django.contrib import admin
from django.contrib import auth

class Ident(models.Model):    
    id = models.BigIntegerField(primary_key=True)
    ident = models.CharField(db_column='ident', max_length=16)
    
    hbbackend = 'hbstats'
    
    class Meta:
        db_table = 'dim_ident'
        managed = False
        ordering = ['ident']

class IdentNamesIterator(object):
    def __init__(self):
        self.objects = None
    def __iter__(self):
        self.objects = iter(Ident.objects.all())
        return self
    def next(self):
        try:
            ident = self.objects.next()
            return (ident.ident, ident.ident)
        except StopIteration:
            raise StopIteration()

class IdentToGroupMapping(models.Model):
    ident = models.CharField(max_length=16, choices=IdentNamesIterator())
    group = models.ForeignKey(auth.models.Group)
    
    def __unicode__(self):
        return 'ident: %s -> group: %s' % (self.ident, self.group.name)

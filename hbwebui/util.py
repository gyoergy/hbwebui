import json
from django.contrib import auth
from models import Ident, IdentToGroupMapping

def dictfetchall(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()
    ]

def dictfetchmany(cursor, rows):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row)) for row in cursor.fetchmany(rows)
    ]

def get_idents(user):
    values_list = IdentToGroupMapping.objects.filter(group=user.groups.all()).values_list('ident')
    
    idents = [ t[0] for t in values_list ]
    
    return idents;

from django.conf.urls import patterns, include, url

urlpatterns = patterns('hbwebui.views',
    url(r'^$', 'dashboard', name='dashboard'),
    url(r'^stats/binaries$', 'stats_binaries', name='stats_binaries'),
    url(r'^stats/source_ips$', 'stats_source_ips', name='stats_source_ips'),
    url(r'^stats/asns$', 'stats_asns', name='stats_asns'),
    url(r'^stats/countries$', 'stats_countries', name='stats_countries'),
    url(r'^stats/idents$', 'stats_idents', name='stats_idents'),
    
    url(r'^stats/binary/([0-9a-z]{32})$', 'stats_binary', name='stats_binary'),
    url(r'^stats/ident/([0-9A-Za-z]{5}@[0-9A-Za-z]{3})$', 'stats_ident', name='stats_ident'),
    url(r'^stats/source_ip/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$', 'stats_source_ip', name='stats_source_ip'),
    url(r'^stats/asn/(\d{1,10})$', 'stats_asn', name='stats_asn'),
    url(r'^stats/country/([A-Z]{2})$', 'stats_country', name='stats_country'),
    
    url(r'^r/stats/binaries$', 'r_stats_binaries', name='r_stats_binaries'),
    url(r'^r/stats/binaries/summary$', 'r_stats_binaries_summary', name='r_stats_binaries_summary'),
    
    url(r'^r/stats/source_ips$', 'r_stats_source_ips', name='r_stats_source_ips'),
    url(r'^r/stats/source_ips/summary$', 'r_stats_source_ips_summary', name='r_stats_source_ips_summary'),
    
    url(r'^r/stats/asns$', 'r_stats_asns', name='r_stats_asns'),
    url(r'^r/stats/asns/summary$', 'r_stats_asns_summary', name='r_stats_asns_summary'),
    
    url(r'^r/stats/countries$', 'r_stats_countries', name='r_stats_countries'),
    url(r'^r/stats/countries/summary$', 'r_stats_countries_summary', name='r_stats_countries_summary'),
    
    url(r'^r/stats/idents$', 'r_stats_idents', name='r_stats_idents'),
    url(r'^r/stats/idents/summary$', 'r_stats_idents_summary', name='r_stats_idents_summary'),
    
    url(r'^r/stats/target_ports$', 'r_stats_target_ports', name='r_stats_target_ports'),
    
        
    url(r'^r/sensoractivity_24', 'sensoractivity', {'hours': 24}, name='sensoractivity_24'),
    url(r'^r/sensoractivity_168', 'sensoractivity', {'hours': 168}, name='sensoractivity_168'),
    
    url(r'^r/ts/sensoractivity_x_168', 'ts_sensoractivity_x', {'hours': 168}, name='ts_sensoractivity_x_168'),    
    
)

urlpatterns += patterns('',
    url(r'^login$', 'django.contrib.auth.views.login',
        {
            'template_name': 'login.html',
        },
        name='login'
    ),
    url(r'^logout$', 'django.contrib.auth.views.logout',
        {
            'next_page': 'login'
        },
        name='logout'
    ),
)
